package com.tbsl.ppwmobile;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class ViewJobScreen extends Activity implements OnClickListener{
	private PpwDatabaseAdapter ppwdatabaseadapter;
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.viewjobscreen);
		View editjobButton = findViewById(R.id.buttonEditJobScreen);
		editjobButton.setOnClickListener(this);
	}

	@Override
	protected void onResume(){
		super.onResume();
		PopulateForm();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
	
	@Override
	public void onClick(View v) {
		Bundle bundle = this.getIntent().getExtras();
		long rowId = bundle.getLong("rowId");
		Log.d("ViewJob","RowID: " + rowId);
		Intent iEditJobScreen = new Intent(this,EditJobScreen.class);
		iEditJobScreen.putExtras(bundle); //Pass bundle on!
		startActivity(iEditJobScreen);
	}


	protected void PopulateForm(){
		Bundle bundle = this.getIntent().getExtras();
		long rowId = bundle.getLong("rowId");
		Log.d("ViewJob","RowID: " + rowId);
		
		ppwdatabaseadapter = new PpwDatabaseAdapter(this);
		ppwdatabaseadapter.open();
		
		PDAJob pdajob = ppwdatabaseadapter.getJob(rowId);
		
		if(pdajob.CD.equals("C")){
			((TextView)findViewById(R.id.textViewCDLabel)).setText("COLLECT");
			((TextView)findViewById(R.id.textViewCDLabel2)).setText("Collect from:");}
		else{
			((TextView)findViewById(R.id.textViewCDLabel2)).setText("Deliver to:");
			((TextView)findViewById(R.id.textViewCDLabel)).setText("DELIVER");}
		
		((TextView)findViewById(R.id.textViewAdr1)).setText(pdajob.Adr1);
		((TextView)findViewById(R.id.textViewAdr2)).setText(pdajob.Adr2);
		((TextView)findViewById(R.id.textViewShortName)).setText(pdajob.ShortName);
		((TextView)findViewById(R.id.textViewTicketID)).setText(Integer.toString(pdajob.TicketID));
		((TextView)findViewById(R.id.textViewCusRef)).setText(pdajob.CusRef1);
		
		((TextView)findViewById(R.id.textViewPostCode)).setText(pdajob.PostCode);
		
		
		if(pdajob.CDDate != null)
		{
			String strDate = pdajob.CDDate;
			Date dtCDDate;
			strDate = strDate.replace("/Date(", "");
			strDate = strDate.replace(")/", "");
			long intDate = Long.parseLong(strDate);
			dtCDDate = new Date();
			dtCDDate.setTime(intDate);
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			strDate = formatter.format(dtCDDate);
			((TextView)findViewById(R.id.textViewDate)).setText(strDate);
		}
		
		if(pdajob.CDTime != null)
		{
			String strDate = pdajob.CDTime;
			Date dtCDTime;
			strDate = strDate.replace("/Date(", "");
			strDate = strDate.replace(")/", "");
			long intDate = Long.parseLong(strDate);
			dtCDTime = new Date();
			dtCDTime.setTime(intDate);
			SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
			strDate = formatter.format(dtCDTime);
			((TextView)findViewById(R.id.textViewCDTime)).setText(strDate);
		}
		
		
		
		
		((TextView)findViewById(R.id.textViewSiteRestrict)).setText(pdajob.SiteRestrict);
		((TextView)findViewById(R.id.textViewOpenTimes)).setText(pdajob.OpenTimes);
		//((TextView)findViewById(R.id.textViewActWt)).setText(Double.toString(pdajob.ActWt));
		((TextView)findViewById(R.id.textViewGoods)).setText(pdajob.Goods);

		((TextView)findViewById(R.id.textViewRemarks)).setText(pdajob.Remarks);
		((TextView)findViewById(R.id.textViewConDetail)).setText(pdajob.ConDetail);
		//((TextView)findViewById(R.id.textViewCondText)).setText(pdajob.Cond);
		((TextView)findViewById(R.id.textViewContainer)).setText(pdajob.Container);
		

		ppwdatabaseadapter.close();

	}
}
