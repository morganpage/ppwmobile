package com.tbsl.ppwmobile;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.w3c.dom.CDATASection;

import com.tbsl.ppwmobile.PpwDatabase.Jobs;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class EditJobScreen extends Activity implements OnClickListener{
	private PpwDatabaseAdapter ppwdatabaseadapter;
	private PDAJob pdajob;
	SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm"); 
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 

    @Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.editjobscreen);
		
		View nextButton = findViewById(R.id.buttonNext);
		nextButton.setOnClickListener(this);
		
		Bundle bundle = this.getIntent().getExtras();
		long rowId = bundle.getLong("rowId");
		Log.d("ViewJob","RowID: " + rowId);
		
		ppwdatabaseadapter = new PpwDatabaseAdapter(this);
		ppwdatabaseadapter.open();
		
		pdajob = ppwdatabaseadapter.getJob(rowId);
		
		if(pdajob.CD.equals("C"))
			((TextView)findViewById(R.id.textViewCDLabel)).setText("COLLECT");
		else
			((TextView)findViewById(R.id.textViewCDLabel)).setText("DELIVER");
		
		((TextView)findViewById(R.id.textViewShortName)).setText(pdajob.ShortName);
		((TextView)findViewById(R.id.textViewTicketID)).setText(Integer.toString(pdajob.TicketID));
		((TextView)findViewById(R.id.textViewCusRef)).setText(pdajob.CusRef1);
		
		((EditText)findViewById(R.id.textViewCondText)).setText(pdajob.Cond);
		((EditText)findViewById(R.id.textViewContainer)).setText(pdajob.Container);
		((EditText)findViewById(R.id.textViewSignedBy)).setText(pdajob.SignedFor);
		
		//Log.d("EditJobScreen","CDArrTime: " + pdajob.CDArrTime);
		
		try {  
			((Button)findViewById(R.id.timePickerArrTime)).setText(timeFormat.format(dateFormat.parse(pdajob.CDArrTime)));
		} catch (Exception e) {
		}
		try {  
			((Button)findViewById(R.id.timePickerDepTime)).setText(timeFormat.format(dateFormat.parse(pdajob.CDDepTime)));
		} catch (Exception e) {
		}
		
		ppwdatabaseadapter.close();
		
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
	public void onArrTimeClick(View v){
		final Calendar calNow = Calendar.getInstance();
		try {
			String strArrTime = ((Button)findViewById(R.id.timePickerArrTime)).getText().toString(); 
		    Date dtArrTime = timeFormat.parse(strArrTime);  
			calNow.set(Calendar.HOUR_OF_DAY, dtArrTime.getHours());
			calNow.set(Calendar.MINUTE, dtArrTime.getMinutes());
		} catch (Exception e) {
		}
		
		TimePickerDialog tp1 = new TimePickerDialog(this, new OnTimeSetListener() {
			@Override
			public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
				// TODO Auto-generated method stub
				final Calendar cal = Calendar.getInstance();
			    cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
			    cal.set(Calendar.MINUTE, minute);
			    //rest of the code
				Button timePickerArrTime;
				timePickerArrTime = (Button)findViewById(R.id.timePickerArrTime);
				timePickerArrTime.setText(timeFormat.format(cal.getTime()));
			}
		}
				, calNow.get(Calendar.HOUR_OF_DAY), calNow.get(Calendar.MINUTE), true);
	    tp1.show();
	}

	public void onDepTimeClick(View v){
		final Calendar calNow = Calendar.getInstance();
		
		try {
			String strDepTime = ((Button)findViewById(R.id.timePickerDepTime)).getText().toString(); 
		    Date dtDepTime = timeFormat.parse(strDepTime);  
			calNow.set(Calendar.HOUR_OF_DAY, dtDepTime.getHours());
			calNow.set(Calendar.MINUTE, dtDepTime.getMinutes());
		} catch (Exception e) {
		}
		
		
		
		TimePickerDialog tp1 = new TimePickerDialog(this, new OnTimeSetListener() {
			@Override
			public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
				// TODO Auto-generated method stub
				final Calendar cal = Calendar.getInstance();
			    cal.set(Calendar.HOUR_OF_DAY, hourOfDay);
			    cal.set(Calendar.MINUTE, minute);
				Button timePickerDepTime;
				timePickerDepTime = (Button)findViewById(R.id.timePickerDepTime);
				timePickerDepTime.setText(timeFormat.format(cal.getTime()));
			}
		}
				
				, calNow.get(Calendar.HOUR_OF_DAY), calNow.get(Calendar.MINUTE), true);
	    tp1.show();
	}
	
	@Override
	public void onClick(View v) {
		Date dtArrTime;Date dtDepTime;
		//Update Job and move to signature screen
		
		pdajob.Cond = ((EditText)findViewById(R.id.textViewCondText)).getText().toString();
		pdajob.Container = ((EditText)findViewById(R.id.textViewContainer)).getText().toString();
		pdajob.SignedFor = ((EditText)findViewById(R.id.textViewSignedBy)).getText().toString();

		
		try {  
			String strArrTime = ((Button)findViewById(R.id.timePickerArrTime)).getText().toString(); 
		    dtArrTime = timeFormat.parse(strArrTime);  
			pdajob.CDArrTime=dateFormat.format(dtArrTime);
		} catch (Exception e) {
			return;
		}
		try {  
			String strDepTime = ((Button)findViewById(R.id.timePickerDepTime)).getText().toString(); 
		    dtDepTime = timeFormat.parse(strDepTime);  
			pdajob.CDDepTime=dateFormat.format(dtDepTime);
			int datecompare = dtArrTime.compareTo(dtDepTime);
			if(datecompare>=1){
				Toast.makeText(getApplicationContext(), "Departed before Arrival?!",Toast.LENGTH_LONG).show();
				return;
			}
			
		} catch (Exception e) {  
			return;
		}
		
		Bundle bundle = this.getIntent().getExtras();
		long rowId = bundle.getLong("rowId");
		Log.d("EditJob","RowID: " + rowId);

		ppwdatabaseadapter = new PpwDatabaseAdapter(this);
		ppwdatabaseadapter.open();
		ppwdatabaseadapter.updateJob(rowId,pdajob);
		
		pdajob = ppwdatabaseadapter.getJob(rowId);
		
		Log.d("EditJob","Cond: " + pdajob.Cond);
		
		
		ppwdatabaseadapter.close();
		
		
		Intent iSignatureScreen = new Intent(this,SignatureScreen.class);
		iSignatureScreen.putExtras(bundle); //Pass bundle on!
		startActivity(iSignatureScreen);
	}

}
