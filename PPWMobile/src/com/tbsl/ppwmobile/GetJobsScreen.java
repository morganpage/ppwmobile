package com.tbsl.ppwmobile;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import com.google.gson.Gson;
import com.tbsl.ppwmobile.PpwDatabase.Jobs;

public class GetJobsScreen extends Activity  implements OnClickListener{
	private PpwDatabaseAdapter ppwdatabaseadapter;
	private ListView listview;
	private Cursor cursor;
	private SimpleCursorAdapter adapter;
	static final String[] FROM = {Jobs._ID,Jobs.CD,Jobs.TICKETID,Jobs.ADR1,Jobs.REMARKS};
	static final int[] TO = {R.id.textView_JobID,R.id.textView_CD,R.id.textView_TicketID,R.id.textView_Adr1,R.id.textView_Remarks};
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.getjobsscreen);
		listview = (ListView) findViewById(R.id.listJobs);
		
		listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,long id) {
				Log.d("GetJobs", "ID:" + id + " Selected:" + position);

				if(position == 0)
					ShowViewJobScreen(id);
				else
					ShowDialog(id);
				
			}});

		View getjobsButton = findViewById(R.id.buttonGetJobs);
        getjobsButton.setOnClickListener(this);
		
		
		ppwdatabaseadapter = new PpwDatabaseAdapter(this);
		ppwdatabaseadapter.open();
		
	}

	public void ShowDialog(final long rowid)
	{
		Log.d("GetJobs","ShowDialloohg");
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("This is not the first job in the list, continue?")
		       .setCancelable(false)
		       .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
		           public void onClick(DialogInterface dialog, int id) {
		        	   ShowViewJobScreen(rowid);
		           }
		       })
		       .setNegativeButton("No", new DialogInterface.OnClickListener() {
		           public void onClick(DialogInterface dialog, int id) {
		                dialog.cancel();
		           }
		       });
		//AlertDialog alert = builder.create();
		builder.show();

	}
	
	public void ShowViewJobScreen(long rowid)
	{
		Bundle bundle = new Bundle();
		bundle.putLong("rowId", rowid);
		Intent iViewJobScreen = new Intent(getApplicationContext(),ViewJobScreen.class);
		iViewJobScreen.putExtras(bundle);
		startActivity(iViewJobScreen);
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		ppwdatabaseadapter.close();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		cursor = ppwdatabaseadapter.getAllJobs();
		startManagingCursor(cursor);
		adapter = new SimpleCursorAdapter(this,R.layout.job_item,cursor,FROM,TO);
		listview.setAdapter(adapter);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId())
		{
			case R.id.buttonGetJobs:
				
				Log.d("GetJobs","button press");
				
				v.setEnabled(false);
				
				
				try {
					new MyAsyncTask().execute("");
				} catch (Exception e) {
					Log.d("GetJobs",e.getLocalizedMessage());
					Toast.makeText(getApplicationContext(), e.getLocalizedMessage(),Toast.LENGTH_LONG).show();
				}
				
				
				
				
				
		}
	}
	
	private InputStream retrieveStream(String url) {
        DefaultHttpClient client = new DefaultHttpClient(); 
        HttpGet getRequest = new HttpGet(url);
        try {
           HttpResponse getResponse = client.execute(getRequest);
           final int statusCode = getResponse.getStatusLine().getStatusCode();
           if (statusCode != HttpStatus.SC_OK) { 
              Log.w(getClass().getSimpleName(), 
                  "Error " + statusCode + " for URL " + url); 
              return null;
           }
           HttpEntity getResponseEntity = getResponse.getEntity();
           return getResponseEntity.getContent();
        } 
        catch (IOException e) {
           getRequest.abort();
           Log.w(getClass().getSimpleName(), "Error for URL " + url, e);
        }
        return null;
     }
	
	private class MyAsyncTask extends AsyncTask<String, Integer, InputStream>
	{
		String strMessage = "";
		@Override
		protected void onProgressUpdate(Integer... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
		}
		@Override
		protected void onPostExecute(InputStream result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			View getjobsButton = findViewById(R.id.buttonGetJobs);
			getjobsButton.setEnabled(true);
			if(result==null)
			{
				Log.d("GetJobs","null source");
				Toast.makeText(getApplicationContext(), "Error getting jobs, do you have a signal?",Toast.LENGTH_LONG).show();
			}
			if(!strMessage.equals(""))
				Toast.makeText(getApplicationContext(), strMessage,Toast.LENGTH_LONG).show();
			adapter = new SimpleCursorAdapter(getApplicationContext(),R.layout.job_item,cursor,FROM,TO);
			listview.setAdapter(adapter);
			//adapter.notifyDataSetChanged();
		}
		@Override
		protected InputStream doInBackground(String... params) {
			
			try {
		        strMessage = ppwdatabaseadapter.SendOutbox();//Send any pending jobs
		        //Toast.makeText(getApplicationContext(), strMessage,Toast.LENGTH_LONG).show();
				String webref = String.format("%s/PPW_WebSite/PDA/Read/?strPDA=%s", ppwdatabaseadapter.getPDAConfig().WEBRef,ppwdatabaseadapter.getPDAConfig().PDAName);
				
				InputStream source = retrieveStream(webref);
				if(source==null)
				{
					Log.d("GetJobs","null source is");
					return null;
				}
				Log.d("GetJobs","Source ok");
		        Gson gson = new Gson();
		        Reader reader = new InputStreamReader(source);
		        PDAJob[] pdajobs = gson.fromJson(reader,PDAJob[].class); // works nicely
		        //Log.d("GetJobs","pdajob:" + pdajob[0].TicketID);
		        //ppwdatabaseadapter.deleteAllJobs();//REMOVE THIS!!!
		        ppwdatabaseadapter.deleteAllLiveJobs();//Makes sure that all local jobs are refreshed with what is on current TD
				ppwdatabaseadapter.insertJobs(pdajobs);
				cursor = ppwdatabaseadapter.getAllJobs();
				startManagingCursor(cursor);
				//adapter = new SimpleCursorAdapter(this,R.layout.job_item,cursor,FROM,TO);
				//listview.setAdapter(adapter);

				return source;
			} catch (Exception e) {
				// TODO: handle exception
				Log.d("GetJobs",e.getLocalizedMessage());
				strMessage = e.getLocalizedMessage();
				//Toast.makeText(getApplicationContext(), e.getLocalizedMessage(),Toast.LENGTH_LONG).show();
				return null;
			}
			
			
		}
		
	}
	
}
