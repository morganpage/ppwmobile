package com.tbsl.ppwmobile;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.impl.client.DefaultHttpClient;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Toast;

public class SignatureScreen  extends Activity implements OnClickListener{
	private PpwDatabaseAdapter ppwdatabaseadapter;
	private PDAJob pdajob;
	

	SignatureView signatureview;
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.signaturescreen);
		
		signatureview = (SignatureView) findViewById(R.id.signatureView);

		View okButton = findViewById(R.id.buttonOK);
        okButton.setOnClickListener(this);

	}

	@Override
	public void onClick(View arg0) {
		
		
		//Save signature to local database and then tick job as ready to send ie in outbox
		Bitmap bitmap = signatureview.GetBitmap();
		if(bitmap == null){
			Toast.makeText(getApplicationContext(), "Blank Signature?!",Toast.LENGTH_LONG).show();
			return;
		}
			
		ByteArrayOutputStream baos = new ByteArrayOutputStream();  
		bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object   
		byte[] bitmapdata = baos.toByteArray();
		
		
		Bundle bundle = this.getIntent().getExtras();
		long rowId = bundle.getLong("rowId");
		Log.d("SigScreen","RowID: " + rowId + " SigLength: " + bitmapdata.length);

		ppwdatabaseadapter = new PpwDatabaseAdapter(this);
		ppwdatabaseadapter.open();
		ppwdatabaseadapter.updateJobSig(rowId,bitmapdata);//Update local database with the signature data
		ppwdatabaseadapter.close();

		new MyAsyncTask().execute(); //Send everything in the outbox

		Intent iPPWMobileActivity = new Intent(this,PPWMobileActivity.class);
		iPPWMobileActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);//flag means that this becomes base activity and all other scrrens cleared 
		startActivity(iPPWMobileActivity);
	}

	
	class MyAsyncTask extends AsyncTask<Void,Void,String>
	{
		@Override
		 protected void onPostExecute(String result) {
			 super.onPostExecute(result);
	         Log.d("SignatureViewTutorialActivity", "Posted");
	 		Toast.makeText(getApplicationContext(), result,Toast.LENGTH_LONG).show();
	     }

		@Override
		protected String doInBackground(Void... params) {
			ppwdatabaseadapter = new PpwDatabaseAdapter(getApplicationContext());
			ppwdatabaseadapter.open();
			String strMessage = ppwdatabaseadapter.SendOutbox();//Send everything that is in the outbox
			ppwdatabaseadapter.close();
			return strMessage;
		}
	
	}

	
}
