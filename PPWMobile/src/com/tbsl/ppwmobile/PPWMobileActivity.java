package com.tbsl.ppwmobile;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;

public class PPWMobileActivity extends Activity implements OnClickListener {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
     // Set up click listeners for all the buttons
        View jobsButton = findViewById(R.id.buttonJobs);
        jobsButton.setOnClickListener(this);

        View configButton = findViewById(R.id.buttonConfig);
        configButton.setOnClickListener(this);

        View systemButton = findViewById(R.id.buttonSystem);
        systemButton.setOnClickListener(this);
        
        
    }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId())
		{
			case R.id.buttonJobs:
				Intent iJobs = new Intent(this,GetJobsScreen.class);
				startActivity(iJobs);
				break;
			case R.id.buttonConfig:
				Log.d("Main","button press config");
				Intent iConfig = new Intent(this,ConfigScreen.class);
				startActivity(iConfig);
				break;
				
		}
	}
}