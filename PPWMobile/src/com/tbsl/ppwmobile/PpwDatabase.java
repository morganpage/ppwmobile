package com.tbsl.ppwmobile;

import android.provider.BaseColumns;

public final class PpwDatabase { //Local database definition
	
	private PpwDatabase() {}
	
	public static final class Jobs implements BaseColumns {
		private Jobs() {}
		
		public static final String TABLE_NAME = "jobs";
		
		public static final String CD = "cd";
	    public static final String TICKETID = "ticketid";
	    public static final String SHORTNAME = "shortname";
	    public static final String ADR1 = "Adr1";
	    public static final String ADR2 ="Adr2";
	    public static final String POSTCODE ="Postcode";
	    public static final String OPENTIMES ="OpenTimes";
	    public static final String SITERESTRICT ="SiteRestrict";
	    public static final String CONTAINER ="Container";
	    public static final String REMARKS ="Remarks";
	    public static final String CONDETAIL ="ConDetail";
	    public static final String GOODS ="Goods";
	    public static final String ACTWT ="ActWt";
	    public static final String CUSREF1 ="CusRef1";
	    public static final String COND ="Cond";
	    public static final String SIGNEDFOR ="SignedFor";
	    public static final String SIGBYTES ="SigBytes";
	    public static final String DELETED ="Deleted";
	    public static final String OUTBOX="Outbox";
	    public static final String SIGNEDTIME="SignedTime";
	    public static final String CDTIME ="CDTime";
	    public static final String CDDATE ="CDDate";
	    
	    //CDArrTime
	    public static final String CDARRTIME ="CDArrTime";
	    public static final String CDDEPTIME ="CDDepTime";
	    
	}

	public static final class PDAConfig implements BaseColumns {
		private PDAConfig() {}
		public static final String TABLE_NAME = "pdaconfig";
		public static final String PDANAME = "pdaname";
	    public static final String PDAPASSWORD = "pdapassword";
	    public static final String WEBREF = "webref";
		
	}
}
