package com.tbsl.ppwmobile;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.tbsl.ppwmobile.PpwDatabase.Jobs;
import com.tbsl.ppwmobile.PpwDatabase.PDAConfig;

public class PpwDatabaseAdapter {
	private static final String DATABASE_NAME = "ppwmobile.db";
	private static final int DATABASE_VERSION = 3;//This must be incremented when adding new fields to db
	private SQLiteDatabase db;
	//private final Context context;
	private PpwDatabaseHelper ppwdatabasehelper;
	
	public PpwDatabaseAdapter(Context _context){
		//context = _context;
		ppwdatabasehelper = new PpwDatabaseHelper(_context);
	}
	
	public PpwDatabaseAdapter open() throws SQLException{
		db = ppwdatabasehelper.getWritableDatabase();
		return this;
	}
	
	public void close() {
		db.close();
	}
	
	public void reset() {
		db.execSQL("DROP DATABASE " + DATABASE_NAME);
	}
	
	
	public void insertJobs(PDAJob[] pdajobs){//Adds or replaces jobs in local db 
		
		if(pdajobs == null) return;
		
		for(int i=0;i<pdajobs.length;i++)
		{
			ContentValues values = new ContentValues();
			PDAJob pdajob = pdajobs[i];
			values.put(Jobs.ACTWT, pdajob.ActWt);
			values.put(Jobs.ADR1, pdajob.Adr1);
			values.put(Jobs.ADR2, pdajob.Adr2);
			values.put(Jobs.CD, pdajob.CD);
			values.put(Jobs.CDTIME, pdajob.CDTime);
			values.put(Jobs.CDDATE, pdajob.CDDate);
			values.put(Jobs.CONDETAIL, pdajob.ConDetail);
			values.put(Jobs.CONTAINER, pdajob.Container);
			values.put(Jobs.CUSREF1, pdajob.CusRef1);
			values.put(Jobs.COND, pdajob.Cond);
			values.put(Jobs.GOODS, pdajob.Goods);
			values.put(Jobs.OPENTIMES, pdajob.OpenTimes);
			values.put(Jobs.POSTCODE, pdajob.PostCode);
			values.put(Jobs.REMARKS, pdajob.Remarks);
			values.put(Jobs.SHORTNAME, pdajob.ShortName);
			values.put(Jobs.SITERESTRICT, pdajob.SiteRestrict);
			values.put(Jobs.TICKETID, pdajob.TicketID);
			values.put(Jobs.SIGNEDFOR, pdajob.SignedFor);
			values.put(Jobs.OUTBOX, false);
			values.put(Jobs.DELETED, false);
			
			values.put(Jobs.CDARRTIME, pdajob.CDArrTime);
			values.put(Jobs.CDDEPTIME, pdajob.CDDepTime);
			
			db.delete(Jobs.TABLE_NAME, "TicketID=" + pdajob.TicketID + " AND CD='" + pdajob.CD + "'",null);
			db.insertOrThrow(Jobs.TABLE_NAME, null, values);
		}
	}
	
	public void deleteAllJobs(){
		db.delete(Jobs.TABLE_NAME, null, null);
	}
	
	public void deleteAllLiveJobs(){
		db.delete(Jobs.TABLE_NAME, "(Deleted = 0) AND (Outbox = 0)", null);
	}
	
	
	public Cursor getAllJobs(){//Returns all non-deleted jobs
		return db.query(Jobs.TABLE_NAME, null, "Deleted = 0", null, null, null, null);
	}
	
	public void updateJobSig(long rowId,byte[] sigbytes){ //Updates signature and sets outbox flag
		ContentValues values = new ContentValues();
		//values.put(Jobs._ID, rowId);
		values.put(Jobs.SIGBYTES, sigbytes);
		values.put(Jobs.OUTBOX, true);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
		Date date = new Date();
		values.put(Jobs.SIGNEDTIME,dateFormat.format(date));
		db.update(Jobs.TABLE_NAME, values, Jobs._ID + "=" + rowId, null);
	}
	
	public String SendOutbox(){
		Cursor cursor = db.query(Jobs.TABLE_NAME, null, "(Outbox <> 0) AND (Deleted = 0)", null, null, null, null);
		cursor.moveToFirst();
		PDAJob pdajob = new PDAJob();
		int successCount=0;int errorCount=0;
        while (cursor.isAfterLast() == false) {
            //view.append("n" + cur.getString(1));
        	//Try to send this job, only amend info that needs to be sent
        	long rowId = cursor.getLong(cursor.getColumnIndex(Jobs._ID));
        	pdajob.TicketID = cursor.getInt(cursor.getColumnIndex(Jobs.TICKETID));
    		pdajob.CD = cursor.getString(cursor.getColumnIndex(Jobs.CD));
        	pdajob.SigBytes = cursor.getBlob(cursor.getColumnIndex(Jobs.SIGBYTES));
    		pdajob.Cond= cursor.getString(cursor.getColumnIndex(Jobs.COND));
    		pdajob.Container = cursor.getString(cursor.getColumnIndex(Jobs.CONTAINER));
    		pdajob.SignedFor= cursor.getString(cursor.getColumnIndex(Jobs.SIGNEDFOR));
    		pdajob.SignedTime= cursor.getString(cursor.getColumnIndex(Jobs.SIGNEDTIME));
    		pdajob.CDArrTime = cursor.getString(cursor.getColumnIndex(Jobs.CDARRTIME));
    		pdajob.CDDepTime = cursor.getString(cursor.getColumnIndex(Jobs.CDDEPTIME));
    		
        	if(SendPDAJob(pdajob))
        	{	//If successful mark this job as deleted
	    		ContentValues values = new ContentValues();
	    		values.put(Jobs.DELETED, true);//Job done!
	    		db.update(Jobs.TABLE_NAME, values, Jobs._ID + "=" + rowId, null);
	    		successCount++;
        	}
        	else
        		errorCount++;
       	    cursor.moveToNext();
        }
        cursor.close();
        
        if(successCount==0 && errorCount==0)
        	return "";
        else
        	return successCount + " job(s) sent. " + errorCount + " failures.";
	}
	
	public Boolean SendPDAJob(PDAJob pdajob){
		try{
			//Log.d("SignatureViewTutorialActivity","Posting, " + bitmapdata.length + " bytes.");
			String webref = String.format("%s/PPW_WebSite/PDA/Write", getPDAConfig().WEBRef);
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(webref);
			MultipartEntity reqEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
			ByteArrayBody bab = new ByteArrayBody(pdajob.SigBytes, "signature.jpg");//Don't think it matters what it is called
			reqEntity.addPart("uploaded",bab);
			
			Gson gson = new Gson();
			pdajob.SigBytes = null;//So it doesn't get sent again in the json!
	        String jsondata = gson.toJson(pdajob, PDAJob.class);
	        
			reqEntity.addPart("jsondata", new StringBody(jsondata,"text/plain",Charset.forName("UTF-8")));
			
			reqEntity.addPart("encrypted_hash", new StringBody(EncryptedHash(),"text/plain",Charset.forName("UTF-8")));
			
			httpPost.setEntity(reqEntity);
			HttpResponse response =httpClient.execute(httpPost);
			BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
			String sResponse;
			StringBuilder s = new StringBuilder();
			while ((sResponse = reader.readLine()) != null) {
				s = s.append(sResponse);
			}
			
			JsonResponse jsonResponse = gson.fromJson(s.toString(), JsonResponse.class);
			
			Log.d("SignatureViewTutorialActivity","Response: " + jsonResponse.Response);
			
			if(jsonResponse.Response.equals("Success"))
				return true;
			else
				return false;
						
		} 
		catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
	}
	
	public String EncryptedHash()
	{
		String secretKey = "Twas brillig, and the slithy toves. Did gyre and gimble in the wabe";
		String strPassword = getPDAConfig().PDAPassword;
		String inputString = strPassword + secretKey;
		return md5(inputString);
	}
	
	public String md5(String s) {
		try {
			MessageDigest m = MessageDigest.getInstance("MD5");
		    m.update(s.getBytes(),0,s.length());
		    String hash = new BigInteger(1, m.digest()).toString(16);
		    return hash;
		} 
		catch (NoSuchAlgorithmException e){
			e.printStackTrace();
            return "";
		}
	}
	
	
	public void updateJob(long rowId,PDAJob pdajob){//Updates a single job keeping rowid
		ContentValues values = new ContentValues();
		//values.put(Jobs._ID, rowId);
		values.put(Jobs.CONTAINER, pdajob.Container);
		values.put(Jobs.COND, pdajob.Cond);
		values.put(Jobs.SIGNEDFOR, pdajob.SignedFor);
		
		values.put(Jobs.CDARRTIME, pdajob.CDArrTime);
		values.put(Jobs.CDDEPTIME, pdajob.CDDepTime);
		
		
		db.update(Jobs.TABLE_NAME, values, Jobs._ID + "=" + rowId, null);
	}
	
	
	public PDAJob getJob(long rowId){
		PDAJob pdajob = new PDAJob();
		Cursor cursor = db.query(true, Jobs.TABLE_NAME, null, Jobs._ID + "=" + rowId, null, null, null, null, null);
		cursor.moveToFirst();
		pdajob.ActWt = cursor.getFloat(cursor.getColumnIndex(Jobs.ACTWT));
		pdajob.Adr1 = cursor.getString(cursor.getColumnIndex(Jobs.ADR1));
		pdajob.Adr2 = cursor.getString(cursor.getColumnIndex(Jobs.ADR2));
		pdajob.CD =  cursor.getString(cursor.getColumnIndex(Jobs.CD));
		pdajob.CDTime =  cursor.getString(cursor.getColumnIndex(Jobs.CDTIME));
		pdajob.CDDate =  cursor.getString(cursor.getColumnIndex(Jobs.CDDATE));
		pdajob.Cond= cursor.getString(cursor.getColumnIndex(Jobs.COND));
		pdajob.ConDetail = cursor.getString(cursor.getColumnIndex(Jobs.CONDETAIL));
		pdajob.Container = cursor.getString(cursor.getColumnIndex(Jobs.CONTAINER));
		pdajob.CusRef1 = cursor.getString(cursor.getColumnIndex(Jobs.CUSREF1));
		pdajob.Goods = cursor.getString(cursor.getColumnIndex(Jobs.GOODS));
		pdajob.OpenTimes = cursor.getString(cursor.getColumnIndex(Jobs.OPENTIMES));
		pdajob.PostCode = cursor.getString(cursor.getColumnIndex(Jobs.POSTCODE));
		pdajob.Remarks = cursor.getString(cursor.getColumnIndex(Jobs.REMARKS));
		pdajob.ShortName= cursor.getString(cursor.getColumnIndex(Jobs.SHORTNAME));
		pdajob.SiteRestrict= cursor.getString(cursor.getColumnIndex(Jobs.SITERESTRICT));
		pdajob.TicketID= cursor.getInt(cursor.getColumnIndex(Jobs.TICKETID));
		//pdajob.Outbox= (Boolean)(cursor.getShort(cursor.getColumnIndex(Jobs.OUTBOX)));
		pdajob.SignedFor= cursor.getString(cursor.getColumnIndex(Jobs.SIGNEDFOR));
		
		pdajob.CDArrTime =  cursor.getString(cursor.getColumnIndex(Jobs.CDARRTIME));
		pdajob.CDDepTime =  cursor.getString(cursor.getColumnIndex(Jobs.CDDEPTIME));

		cursor.close();
		return pdajob;
	}
	
	
	//public string get
	
	public void updatePDAConfig(PDAConfiguration pdaconfig){
		db.delete(PDAConfig.TABLE_NAME, null, null);
		ContentValues values = new ContentValues();
		values.put(PDAConfig.PDANAME,pdaconfig.PDAName);
		values.put(PDAConfig.PDAPASSWORD, pdaconfig.PDAPassword);
		values.put(PDAConfig.WEBREF, pdaconfig.WEBRef);
		db.insertOrThrow(PDAConfig.TABLE_NAME, null, values);
	}
	
	public PDAConfiguration getPDAConfig(){
		PDAConfiguration pdaconfig = new PDAConfiguration();
		Cursor cursor;
		cursor = db.query(PDAConfig.TABLE_NAME, null, null, null, null, null, null);
		cursor.moveToFirst();
		pdaconfig.PDAName = cursor.getString(cursor.getColumnIndex(PDAConfig.PDANAME));
		pdaconfig.PDAPassword = cursor.getString(cursor.getColumnIndex(PDAConfig.PDAPASSWORD));
		pdaconfig.WEBRef = cursor.getString(cursor.getColumnIndex(PDAConfig.WEBREF));
		cursor.close();
		return pdaconfig;
	}
	
	
	private static class PpwDatabaseHelper extends SQLiteOpenHelper{
	
		
		public PpwDatabaseHelper(Context context) {
			super(context, DATABASE_NAME , null, DATABASE_VERSION);
		}
	
		@Override
		public void onCreate(SQLiteDatabase db) {
			// TODO Auto-generated method stub
			db.execSQL("CREATE TABLE " + Jobs.TABLE_NAME + "("
					+ Jobs._ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
					+ Jobs.CD + " TEXT," 
					+ Jobs.TICKETID + " TEXT,"
					+ Jobs.SHORTNAME + " TEXT,"
					+ Jobs.ADR1 + " TEXT,"
					+ Jobs.ADR2 + " TEXT,"
					+ Jobs.POSTCODE + " TEXT,"
					+ Jobs.OPENTIMES + " TEXT,"
					+ Jobs.SITERESTRICT + " TEXT,"
					+ Jobs.CONTAINER + " TEXT,"
					+ Jobs.REMARKS + " TEXT,"
					+ Jobs.CONDETAIL + " TEXT,"
					+ Jobs.GOODS + " TEXT,"
					+ Jobs.ACTWT + " TEXT,"
					+ Jobs.CUSREF1 + " TEXT,"
					+ Jobs.COND + " TEXT,"
					+ Jobs.SIGNEDFOR + " TEXT,"
					+ Jobs.SIGBYTES + " TEXT,"
					+ Jobs.DELETED + " TEXT,"
					+ Jobs.OUTBOX + " TEXT,"
					+ Jobs.SIGNEDTIME + " TEXT,"
					+ Jobs.CDTIME + " TEXT,"
					+ Jobs.CDDATE + " TEXT,"
					+ Jobs.CDARRTIME + " TEXT,"
					+ Jobs.CDDEPTIME + " TEXT"
					+ ");"
					);
			
			db.execSQL("CREATE TABLE " + PDAConfig.TABLE_NAME + "("
					+ PDAConfig._ID  + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
					+ PDAConfig.PDANAME + " TEXT,"
					+ PDAConfig.PDAPASSWORD + " TEXT,"
					+ PDAConfig.WEBREF + " TEXT"
					+ ");"
					);
			ContentValues values = new ContentValues();
			values.put(PDAConfig.PDANAME, "PDA1");
			values.put(PDAConfig.PDAPASSWORD, "duckbill");
			values.put(PDAConfig.WEBREF, "https://remote.cisindustrial.co.uk");
			db.insertOrThrow(PDAConfig.TABLE_NAME, null, values);
		}
	
		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub
			//Just recreate db
			Log.d("PPWDatabaseadapter","Upgrade from:" + oldVersion + "To:" + newVersion);
			//if (newVersion > 1)	db.execSQL("ALTER TABLE " + Jobs.TABLE_NAME + " ADD COLUMN " + Jobs.CDDATE + " TEXT");
		}
	
	}
}