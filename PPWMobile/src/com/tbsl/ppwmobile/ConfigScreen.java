package com.tbsl.ppwmobile;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Toast;

public class ConfigScreen extends Activity implements OnClickListener {
	private EditText editTextPDAName;
	private EditText editTextPDAPassword;
	private EditText editTextWEBRef;
	private PDAConfiguration pdaconfig;
	private PpwDatabaseAdapter ppwdatabaseadapter;

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.configscreen);
        
        //setTitle("Config Screen");
        
     // Set up click listeners for all the buttons
        View buttonSave = findViewById(R.id.buttonSave);
        buttonSave.setOnClickListener(this);
        editTextPDAName = (EditText)findViewById(R.id.editTextPDAName);
        editTextPDAPassword = (EditText)findViewById(R.id.editTextPDAPassword);
        editTextWEBRef = (EditText)findViewById(R.id.editTextWEBRef);

        ppwdatabaseadapter = new PpwDatabaseAdapter(this);
		ppwdatabaseadapter.open();
        pdaconfig = ppwdatabaseadapter.getPDAConfig();
        editTextPDAName.setText(pdaconfig.PDAName);
        editTextPDAPassword.setText(pdaconfig.PDAPassword);
        editTextWEBRef.setText(pdaconfig.WEBRef);
    }

	

	@Override
	public void onClick(View arg0) {
		// SAVE
		Log.d("ConfigScreen", "Click button" );
		pdaconfig.PDAName =editTextPDAName.getText().toString();
		pdaconfig.PDAPassword=editTextPDAPassword.getText().toString();
		pdaconfig.WEBRef=editTextWEBRef.getText().toString();
		ppwdatabaseadapter.updatePDAConfig(pdaconfig);
		Toast.makeText(getApplicationContext(), "Saved.",Toast.LENGTH_LONG).show();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		ppwdatabaseadapter.close();
	}
}
