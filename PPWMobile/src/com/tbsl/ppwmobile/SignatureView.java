package com.tbsl.ppwmobile;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class SignatureView extends View {
	private Bitmap  mBitmap;
    private Canvas  mCanvas;
    private Path    mPath;
    private Paint   mBitmapPaint;
    private Paint       mPaint;
    private Boolean touched;
    
    public SignatureView(Context context) {
		super(context);
		initSignatureView();
	}

	public SignatureView(Context context,AttributeSet attrs) {
		super(context,attrs);
		initSignatureView();
	}

	public SignatureView(Context context,AttributeSet attrs,int defaultStyle) {
		super(context,attrs,defaultStyle);
		initSignatureView();
	}

	public Bitmap GetBitmap()
	{
		if(touched)
			return mBitmap;
		else
			return null;
	}
	
	protected void initSignatureView(){
		
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setColor(Color.BLACK);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(8);
        touched=false;
	}
	
	 @Override
	    public void onDraw(Canvas canvas) {
		 
		 if(mBitmap == null)
		 {
			 Log.d("SignatureView","Height: " + getMeasuredHeight() + "Width: " + getMeasuredWidth());
			 mBitmap = Bitmap.createBitmap( getMeasuredWidth(), getMeasuredHeight(),Bitmap.Config.ARGB_8888);
			 mBitmap.eraseColor(Color.WHITE); //If left transparent, turns up black on server
			 mCanvas = new Canvas(mBitmap);
			 mPath = new Path();
			 mBitmapPaint = new Paint(Paint.DITHER_FLAG);
		 }
		 
		 canvas.drawColor(0xFFAAAAAA);
		 canvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);
		 canvas.drawPath(mPath, mPaint);
		 
	    }
	 
	 
	 @Override
     public boolean onTouchEvent(MotionEvent event) {
		 touched=true	;
         float x = event.getX();
         float y = event.getY();
         
         switch (event.getAction()) {
             case MotionEvent.ACTION_DOWN:
                 touch_start(x, y);
                 invalidate();
                 break;
             case MotionEvent.ACTION_MOVE:
                 touch_move(x, y);
                 invalidate();
                 break;
             case MotionEvent.ACTION_UP:
                 touch_up();
                 invalidate();
                 break;
         }
         return true;
     }
	 
	 
	 private float mX, mY;
     private static final float TOUCH_TOLERANCE = 4;
	 
	 private void touch_start(float x, float y) {
         mPath.reset();
         mPath.moveTo(x, y);
         mX = x;
         mY = y;
     }
     private void touch_move(float x, float y) {
         float dx = Math.abs(x - mX);
         float dy = Math.abs(y - mY);
         if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
             mPath.quadTo(mX, mY, (x + mX)/2, (y + mY)/2);
             mX = x;
             mY = y;
         }
     }
     private void touch_up() {
         mPath.lineTo(mX, mY);
         // commit the path to our offscreen
         mCanvas.drawPath(mPath, mPaint);
         // kill this so we don't double draw
         mPath.reset();
     }
     
//     @Override
//     protected void onMeasure(int wMeasureSpec, int hMeasureSpec) {
//	     int measuredHeight = measureHeight(hMeasureSpec);
//	     int measuredWidth = measureWidth(wMeasureSpec);
//	     // MUST make this call to setMeasuredDimension
//	     // or you will cause a runtime exception when
//	     // the control is laid out.
//	     setMeasuredDimension(measuredHeight, measuredWidth);
//     }
//     private int measureHeight(int measureSpec) {
//	     int specMode = MeasureSpec.getMode(measureSpec);
//	     int specSize = MeasureSpec.getSize(measureSpec);
//	     return specSize;
//     }
//     private int measureWidth(int measureSpec) {
//	     int specMode = MeasureSpec.getMode(measureSpec);
//	     int specSize = MeasureSpec.getSize(measureSpec);
//	     return specSize;
//     }     
//     
     
     
     
}
